$(document).ready(function () {
    $("body").on("click", ".small__menu-btn", function () {
        $(".small__menu").slideToggle();
        return false;
    });

    $("body").on("click", ".user-name", function () {
        $(".user-actions").slideToggle();
        $(".user-name").toggleClass('user-name-arrow-down');

        return false;
    });

    $("body").on("click", ".basket__link", function () {
        if($(window).width() > 740) {
            $(".header__bottom_basket").toggleClass('header__bottom_basket-opened');
            $(".basket-layer").toggleClass("basket-layer-visible");
            return false;
        }
    });

    $(window).resize(function() {
        if($(window).width() <= 740) {
            $(".header__bottom_basket").removeClass('header__bottom_basket-opened');
            $(".basket-layer").removeClass("basket-layer-visible");
        }
    })

    $("body").on("click", ".cart_x", function () {
        $(this).parents('tr.even').remove();
    });

    $(".header-basket-items-list-container").slimScroll({
        height: '200px'
    });

    $("#feedback__price_form-phone").mask("+7 (999) 999-99-99", {placeholder: "+7 (___) ___-__-__"}); //
    $("#feedback__price_form-phone").on('dblclick',function() {
        var v=$(this).mask();
        $(this).unmask();
        $(this).val("+7 " + v);
    })

});
